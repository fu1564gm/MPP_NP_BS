#include "aufgabe.h"
#include "stm32f4xx_gpio.h"
#include "stdio.h"
#include "stdlib.h"
#include "test.h"
#include "usart.h"

bool b;
bool c;
uint8_t d;
uint8_t counter;
bool blockedState;

char usart2_rx_buffer[50];
char usart2_tx_buffer[50];
int i=0;
int laenge=0;
char character;
char uart2_RX_character1 = 0;
unsigned char usart2_running1 = 0;


void init_leds(void)
{
	// enable clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	// Change pin 2: Mode, Speed, OType and PuPd
	GPIO_InitTypeDef gpiob_init = {
		.GPIO_Pin   = GPIO_Pin_2,
		.GPIO_Mode  = GPIO_Mode_OUT,
		.GPIO_Speed = GPIO_Speed_25MHz,
		.GPIO_OType = GPIO_OType_PP,
		.GPIO_PuPd  = GPIO_PuPd_NOPULL,
	};
	GPIO_Init(GPIOB, &gpiob_init);

	GR_LED_ON;
}
void led_set(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_2);
}


void led_reset(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_2);
}


void led_toggle(void)
{
	GPIO_ToggleBits(GPIOB, GPIO_Pin_2);
}
void blinkleds(void)
{
	init_leds();
	while (1) {
		GR_LED_TOGGLE();
		wait_uSek(10000);
	}
}
void init_taste_2(void)
{
	// Enable GPIOC clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	// Enable SYSCFG clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	GPIO_InitTypeDef gpioc_init = {
		.GPIO_Pin = GPIO_Pin_5,
		.GPIO_Mode = GPIO_Mode_IN,
		.GPIO_Speed = GPIO_Speed_25MHz,
		.GPIO_OType = GPIO_OType_PP,
		.GPIO_PuPd = GPIO_PuPd_NOPULL,
	};
	GPIO_Init(GPIOC, &gpioc_init);


}

void init_taste_1(void)
{
	// Enable GPIOC clock
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	// Enable SYSCFG clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	GPIO_InitTypeDef gpioc_init = {
		.GPIO_Pin = GPIO_Pin_8,
		.GPIO_Mode = GPIO_Mode_IN,
		.GPIO_Speed = GPIO_Speed_25MHz,
		.GPIO_OType = GPIO_OType_PP,
		.GPIO_PuPd = GPIO_PuPd_NOPULL,
	};
	GPIO_Init(GPIOC, &gpioc_init);

}
void anundaus(void)
{

	b = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8);
	c = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5);
	if (b==0)
	{
		GR_LED_OFF;
		counter = 2;
		blockedState = false;
	}
	else if (c==1)
	{
		if ( blockedState == false){
			counter = counter - 1;
			delay__ms(1000);
			if (counter == 0)
			{	GR_LED_ON;
				counter = 2;
			}
			else
			{
				blockedState = true;
			}
		}
	}
	else
	{
		blockedState = false;
	}
}
void aufgabe_A03_1_1(void)
{
    int i = 0;
    char out[20] = {0};

    while(1)
    {
        i++;
        sprintf(out,"i=%d\r\n",i);
        USART_SendData(USART2, out);
        if ( i>9){ i=0;}
        wait_mSek(1000);
    }
}
void init_PC09() {
    // Setzt GPIO Port auf den Reset Zustand zur�ck
    GPIO_DeInit(GPIOC);

    // Taktquelle f�r die Peripherie aktivieren
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    // Struct anlegen
    GPIO_InitTypeDef GPIO_InitStructure;

    // Struct Initialisieren setzt alle Leitungen auf
    // Eingang ohne PushPull
    GPIO_StructInit(&GPIO_InitStructure);

    // Benutze Pin 9 gem�� Aufgabe
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;

    //
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

    // Setze den Modus auf Push/Pull
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

    // Verwende NoPull
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

    // Setze die Frequenz des Ports auf 50 MHz
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    // Initialisiere den GPIO Port
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    //
    GPIO_PinAFConfig(GPIOC, GPIO_Pin_9, GPIO_AF_MCO);

    //
    RCC_MCO2Config(RCC_MCO2Source_SYSCLK, RCC_MCO2Div_1);
}

//==== Taktfrequenz 24MHz mit HSE-OSC=16MHz
static void slowMode(void) {
    RCC_DeInit();

    RCC_HSEConfig(RCC_HSE_ON);
    if (RCC_WaitForHSEStartUp() == ERROR) {
        return;
    }
    // HSEOSC=16MHz SYSCLK=24MHz HCLK=24MHz
    // PCLK1=24 PCLK2=24MHz
    RCC_PLLConfig(RCC_PLLSource_HSE,    //RCC_PLLSource
                                16,     //PLLM
                                192,    //PLLN
                                8,      //PLLP
                                4       //PLLQ
                                );
    RCC_PLLCmd(ENABLE);
    RCC_WaitForPLLStartUp();

    // Configures the AHB clock (HCLK)
    RCC_HCLKConfig(RCC_SYSCLK_Div1);
    // Low Speed APB clock (PCLK1)
    RCC_PCLK1Config(RCC_HCLK_Div1);
    // High Speed APB clock (PCLK2)
    RCC_PCLK2Config(RCC_HCLK_Div1);

    // select system clock source
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}


//==== Taktfrequenz 168MHz mit HSE-OSC=16MHz
static void fastMode(void) {
    RCC_DeInit();

    RCC_HSEConfig(RCC_HSE_ON);
    if (RCC_WaitForHSEStartUp() == ERROR) {
        return;
    }
    // HSEOSC=16MHz SYSCLK=168MHz HCLK=168MHz
    // PCLK1=42MHz PCLK2=84MHz
    RCC_PLLConfig(RCC_PLLSource_HSE,    //RCC_PLLSource
                                16,     //PLLM
                                336,    //PLLN
                                2,      //PLLP
                                7       //PLLQ
                                );
    RCC_PLLCmd(ENABLE);
    RCC_WaitForPLLStartUp();

    // Configures the AHB clock (HCLK)
    RCC_HCLKConfig(RCC_SYSCLK_Div1);
    // High Speed APB clock (PCLK1)
    RCC_PCLK1Config(RCC_HCLK_Div4);
    // High Speed APB clock (PCLK2)
    RCC_PCLK2Config(RCC_HCLK_Div2);


    // select system clock source
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}
void fastandslow(void)
{

	b = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8);
	c = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_5);
	if (b==0)
	{ slowMode();
	}
	else if (c==1)
	{ fastMode();
	}
}
void led_control(void)
{
	// warten bis ein character empfangen wurde
	if (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) != RESET)
		{// character lesen
		character = (char)USART_ReceiveData(USART2);
		switch(character) {
			case '1': usart_2_print("gruene LED im 1 Sekundentakt\r\n");
				mytime = 1000;
				break;
			case '2': usart_2_print("gruene LED im 2 Sekundentakt\r\n");
				mytime = 2000;
				break;
			case '4': usart_2_print("gruene LED im 4 Sekundentakt\r\n");
				mytime = 4000;
				break;
			}

		}
	led_timer(mytime);

}
void led_timer(int msek)
{
	wait_mSek(mytime);
	GR_LED_Toggle;

}
void init_usart_2_tx(void)
{
	// Struct Anlegen
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// Peripherie Clocksystem Einschalten
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);


	// GPIO Initialisieren
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Alternativ Funktion Zuweisen
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);

	// USART als Alternativ Funktion Initialisieren
	USART_InitStructure.USART_BaudRate = 921600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);

	// USART Einschalten
	USART_Cmd(USART2, ENABLE);
}
void init_usart_2(void)
{
	// Struct Anlegen
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// Peripherie Clocksystem Einschalten
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);


	// GPIO Initialisieren
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Alternativ Funktion Zuweisen
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	// USART als Alternativ Funktion Initialisieren
	USART_InitStructure.USART_BaudRate = 921600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init(USART2, &USART_InitStructure);

	// USART Einschalten
	USART_Cmd(USART2, ENABLE);
}
////////////////////Aufgabe 4
void init_iwdg()
{
// Schreibrechte aktivieren
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
// den Vorteiler (4, 8 , 16 ,..., 256) setzen
    IWDG_SetPrescaler(IWDG_Prescaler_64);
// den Wert (0...4095) einstellen ab dem runtergez�hlt wird
    IWDG_SetReload(2500);
// setzt den Wachdog auf den eingestellten Maximalwert
    IWDG_ReloadCounter();
// aktiviert dem IWDG
    IWDG_Enable();
// Das Zeitintervall t berechnet sich folgenderma�en
// t = (1/32000) * 64 * 2500 = 5 Sekunden
}

void usart_2_print(char* chars)
{
	int i;
	for(i = 0;i < strlen(chars);i++)
	{
		USART_SendData(USART2, chars[i]);
		/* Checke die Transfer Complete Flag */
		while (USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET){}
	}
}
void char_laenge()
{// Checken �ber Eingabe

    	        while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) != RESET)
    	        {
    	            // speichern der Eingabe
    	            character = (char) USART_ReceiveData(USART2);

    	            // checke ob Carriage Return
    	            if (character == '\r')
    	            {
    	            	//usart2_rx_buffer[i] = 0x00;
    	            	// Erstelle output
    	                sprintf(usart2_tx_buffer, "chars=%s Laenge=%d\r\n", usart2_rx_buffer, laenge);
    	                usart_2_print(usart2_tx_buffer);
    	                // buffer leeren
    	                memset(usart2_rx_buffer,0x00,20);

    	                i = 0;
    	                laenge = 0;
    	            } else {
    	                // character in den Buffer legen
    	                // erhoehe die Laufvariablen
    	                usart2_rx_buffer[i] = character;
    	                laenge++;
    	                i = (i + 1) % 50;
    	            }
    	        }
}
///////////////////////Aufgabe 5
void init_taste_1_irq()
{
	init_taste_1();
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);

	EXTI_InitTypeDef EXTI_InitStructure;

	EXTI_InitStructure.EXTI_Line = EXTI_Line8;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;

	EXTI_Init(&EXTI_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);
}
void init_taste_2_irq()
{
	init_taste_2();
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource5);

	EXTI_InitTypeDef EXTI_InitStructure;

	EXTI_InitStructure.EXTI_Line = EXTI_Line5;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;

	EXTI_Init(&EXTI_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);

}
void disable_exti()
{
		//==========================================================
        //========= Interrupt Konfiguration
        //==========================================================
        // Bindet Port C Leitung 8 an die EXTI_Line8 Leitung
        SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);

        // Struct anlegen
        EXTI_InitTypeDef EXTI_InitStructure;

        // EXTI_Line zuweisen
        EXTI_InitStructure.EXTI_Line = EXTI_Line8;

        // Interrupt Mode setzen
        EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;

        // Triggerbedingung setzen
        EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;

        // Interrupt verbieten
        EXTI_InitStructure.EXTI_LineCmd = DISABLE;

        // Register aus dem Struct heraus setzen
        EXTI_Init(&EXTI_InitStructure);

        EXTI_ClearFlag(EXTI_Line8);
        EXTI_ClearITPendingBit(EXTI_Line8);
}
void init_usart_2_irq()
{
	init_usart_2();

	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Freigabe des Interrupts
	USART_ClearITPendingBit(USART2, USART_IT_RXNE);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}
////////////////////// Aufgabe 6
void zeitunddatum()
{
	RTC_TimeTypeDef  RTC_Time_Aktuell;      //  Zeit
	RTC_DateTypeDef  RTC_Date_Aktuell;      //  Datum
	RTC_AlarmTypeDef RTC_Alarm_Aktuell;     //  Alarm

	char data[50] = {0};
	// Datum
        RTC_GetDate(RTC_Format_BIN, &RTC_Date_Aktuell);
        sprintf(data,"\r\n%.2d-%.2d-%.2d",RTC_Date_Aktuell.RTC_Date, RTC_Date_Aktuell.RTC_Month, RTC_Date_Aktuell.RTC_Year);
        usart_2_print(data);

        // Zeit
        RTC_GetTime(RTC_Format_BIN, &RTC_Time_Aktuell);
        sprintf(data,"\r\n%.2d:%.2d:%.2d",RTC_Time_Aktuell.RTC_Hours, RTC_Time_Aktuell.RTC_Minutes, RTC_Time_Aktuell.RTC_Seconds);
        usart_2_print(data);

        wait_mSek(1000);
}
void zeitunddatum_einstellen()
{
		char buff2[50];
		RTC_DateTypeDef RTC_Date_Struct;
		RTC_TimeTypeDef RTC_Time_Struct;
		// Eingabe YYMMDD-HHMMSS
		// -48, da ints um 48 erh�ht sind (ascii)
		RTC_DateStructInit(&RTC_Date_Struct);
		RTC_Date_Struct.RTC_Year = ((usart2_rx_buffer[0])-48)*16 + ((usart2_rx_buffer[1])-48);
		RTC_Date_Struct.RTC_Month = ((usart2_rx_buffer[2])-48)*16 + ((usart2_rx_buffer[3])-48);
		RTC_Date_Struct.RTC_Date = ((usart2_rx_buffer[4])-48)*16 + ((usart2_rx_buffer[5])-48);
		RTC_SetDate(RTC_Format_BCD, &RTC_Date_Struct);

		RTC_TimeStructInit(&RTC_Time_Struct);
		RTC_Time_Struct.RTC_Hours = ((usart2_rx_buffer[7])-48)*16 + ((usart2_rx_buffer[8])-48);
		RTC_Time_Struct.RTC_Minutes = ((usart2_rx_buffer[9])-48)*16 + ((usart2_rx_buffer[10])-48);
		RTC_Time_Struct.RTC_Seconds = ((usart2_rx_buffer[11])-48)*16 + ((usart2_rx_buffer[12])-48);
		RTC_SetTime(RTC_Format_BCD, &RTC_Time_Struct);
		sprintf(buff2,"rx after=%s \r\n",usart2_rx_buffer);
		usart_2_print(buff2);
}
void init_rtc_alarm()
{
	// RTC Alarm A Interruptkonfiguration

	// Anlegen der ben�tigten Structs
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	// EXTI-Line Konfiguration
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	// NIVC Konfiguration
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Konfigurieren des Alarm A
	RTC_ITConfig(RTC_IT_ALRA, ENABLE);

	// RTC Alarm A freigeben
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

	// Alarmflag l�schen
	RTC_ClearFlag(RTC_FLAG_ALRAF);
}
///////////in interrupt.c
void alarm_1()
{
	RTC_AlarmTypeDef RTC_Alarm_Struct;

	// Alarmzeit setze
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_H12      = RTC_H12_AM;
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours    = 0x00;
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes  = 0x00;
	RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds  = 0x30;

	// Alarmmaske
	RTC_Alarm_Struct.RTC_AlarmMask =  RTC_AlarmMask_DateWeekDay // Wochentag oder Tag ausgeblendet
	                                | RTC_AlarmMask_Hours       // Stunde ausgeblendet
	                                | RTC_AlarmMask_Minutes;    // Minute ausgeblendet


	// Alarm f�r den Tag oder Wochentag ausw�hlen
	RTC_Alarm_Struct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date; // Tag (1-31)
	// Alarm Tag oder Wochentag setzen
	RTC_Alarm_Struct.RTC_AlarmDateWeekDay    = 0x02; // current Date
	// Konfiguration von Alarm A
	RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_Alarm_Struct);
}
void alarm_2()
{
	{
		RTC_AlarmTypeDef RTC_Alarm_Struct;
		// Alarmzeit setzen
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_H12      = RTC_H12_AM;
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours = 0x00;
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes = 0x00;
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds = 0x30;

		// Alarm f�r den Tag oder Wochentag ausw�hlen
		RTC_Alarm_Struct.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_WeekDay; // Tag (1-31)
		// Alarm Tag oder Wochentag setzen
		RTC_Alarm_Struct.RTC_AlarmDateWeekDay    = RTC_Weekday_Monday; //current Date
		// Konfiguration von Alarm A
		RTC_SetAlarm(RTC_Format_BCD, RTC_Alarm_A, &RTC_Alarm_Struct);

	}
}
void alarm_3()
{
	{
		RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

		RTC_TimeTypeDef  RTC_Time_Aktuell;      //  Zeit
		RTC_AlarmTypeDef RTC_Alarm_Struct;

		RTC_GetTime(RTC_Format_BIN, &RTC_Time_Aktuell);	// current time

		// Alarmzeit setze
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_Hours = RTC_Time_Aktuell.RTC_Hours;
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_Minutes = RTC_Time_Aktuell.RTC_Minutes;
		RTC_Alarm_Struct.RTC_AlarmTime.RTC_Seconds = (((RTC_Time_Aktuell.RTC_Seconds) + 25)% 60);  // aktuelle sekunden +25 mod 60, da eine Minute =60 Sekunden

		// Alarmmaske
		RTC_Alarm_Struct.RTC_AlarmMask          = RTC_AlarmMask_DateWeekDay
														| RTC_AlarmMask_Hours
														| RTC_AlarmMask_Minutes;
		// Konfiguration von Alarm A
		RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_Alarm_Struct);

		// RTC Alarm A Interruptkonfiguration

		// Anlegen der ben�tigten Structs
		EXTI_InitTypeDef EXTI_InitStructure;
		NVIC_InitTypeDef NVIC_InitStructure;

		// EXTI-Line Konfiguration
		EXTI_ClearITPendingBit(EXTI_Line17);
		EXTI_InitStructure.EXTI_Line = EXTI_Line17;
		EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
		EXTI_InitStructure.EXTI_LineCmd = ENABLE;
		EXTI_Init(&EXTI_InitStructure);

		// NIVC Konfiguration
		NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);

		// Konfigurieren des Alarm A
		RTC_ITConfig(RTC_IT_ALRA, ENABLE);

		// RTC Alarm A freigeben
		RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

		// Alarmflag l�schen
		RTC_ClearFlag(RTC_FLAG_ALRAF);

	}
}


void init_rtc_wake_up(){
	 // Strukt anlegen
	    EXTI_InitTypeDef EXTI_InitStructure;
	    NVIC_InitTypeDef NVIC_InitStructure;

	    // EXTI-Line Konfiguration f�r WakeUp
	    EXTI_ClearITPendingBit(EXTI_Line22);
	    EXTI_InitStructure.EXTI_Line = EXTI_Line22;
	    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	    EXTI_Init(&EXTI_InitStructure);

	    // NIVC Konfiguration f�r WakeUp
	    NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
	    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	    NVIC_Init(&NVIC_InitStructure);

	    RTC_WakeUpCmd(DISABLE);

	    // Konfiguration der Clock
	    // RTC_WakeUpClock_RTCCLK_Div2;    (122,070usek...4sek) Zeitbasis: 61,035us
	    // RTC_WakeUpClock_RTCCLK_Div4;    (244,140usek...8sek) Zeitbasis: 122,070us
	    // RTC_WakeUpClock_RTCCLK_Div8;    (488,281usek...16sek)Zeitbasis: 244,140us
	    // RTC_WakeUpClock_RTCCLK_Div16;   (976,562usek...32sek)Zeitbasis: 488,281us
	    // RTC_WakeUpClock_CK_SPRE_16bits; (1sek...65535sek)    Zeitbasis: 1s
	    // RTC_WakeUpClock_CK_SPRE_17bits: (1sek...131070sek)   Zeitbasis: 1s
	    RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div2);

	    // RTC_WakeUpCouter mit einem Wert zwischen 0x0000...0xFFFF setzen
	    // Wert des WakeUpCounter aus Zeitintervall/Zeitbasis berechnen
	    // WakeUpCounterwert f�r intervall von 2s bei RTC_WakeUpClock_RTCCLK_Div2
	    // ergibt sich aus 2s/61,035us = 32768
	    RTC_SetWakeUpCounter(32768);
}
void wake_up(){
	// Disable RTC wakeup interrupt
	    RTC_ITConfig(RTC_IT_WUT,DISABLE);

	    // Clear PWR Wakeup WUF Flag
	    PWR_ClearFlag(PWR_CSR_WUF);
	    PWR_WakeUpPinCmd(ENABLE);

	    // Clear RTC Wakeup WUTF Flag
	    RTC_ClearITPendingBit(RTC_IT_WUT);
	    RTC_ClearFlag(RTC_FLAG_WUTF);

	    // Freigeben
	    RTC_ITConfig(RTC_IT_WUT, ENABLE);   // Bit 14
	    RTC_AlarmCmd(RTC_CR_WUTE, ENABLE);  // Bit 10

	    RTC_WakeUpCmd(ENABLE);
}
